
from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash
from contextlib import closing
import mysql.connector


# create app
app = Flask(__name__)
app.config["DEBUG"]=True


# apis

def upload_user_request(url, genre, email):
    cnx = mysql.connector.connect(user='rippletrack', password='circulately',
                                  host='trackripple.chq9fsysngsi.us-east-1.rds.amazonaws.com',
                                  port = '3306', database = 'trackripple')
        
    cursor = cnx.cursor(buffered = True)
   
        
    query = 'INSERT INTO user_requests (song_url, genre, email) VALUES (%s, %s, %s)'
    row_data= (url,genre,email)
    cursor.execute(query,row_data)
        
    cnx.commit()
        
    cursor.close()
    cnx.close()
    return



@app.route('/',methods=['POST','GET'])
def index():
    if request.method == 'POST':
        upload_user_request(request.form.get('song'),request.form.get('genre'),request.form.get('email'))
    return render_template('index.html')


@app.route('/payments')
def payments():
    return render_template('payment.html')

if __name__ == '__main__':
    app.debug=True
    app.run()
